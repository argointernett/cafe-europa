'use strict';

var app = (function(document, $) {
	var docElem = document.documentElement,
		_userAgentInit = function() {
			docElem.setAttribute('data-useragent', navigator.userAgent);
		},
		_init = function() {
			$(document).foundation();
			// needed to use joyride
			// doc: http://foundation.zurb.com/docs/components/joyride.html
			$(document).on('click', '#start-jr', function () {
				$(document).foundation('joyride', 'start');
			});
			_userAgentInit();
		};
	return {
		init: _init
	};
})(document, jQuery);

(function() {
	app.init();
})();

// Scroll
	$(window).scroll(function(){
		var st = $(this).scrollTop();
		if( st >= 10){
			$('#top').addClass('minimized');
		}else{
			$('#top').removeClass('minimized');
		}
	});

// Add this class for when the website loads in the middle of the page
	if( $(window).scrollTop() >= 10)
	{
		$('#top').addClass('minimized');
	}

// BXslider

	$('.bxslider').bxSlider({
		mode: 'fade',
		controls: true,
		auto: ($(".bxslider li").length > 1) ? true: false,
		pager: ($(".bxslider li").length > 1) ? true: false,
		speed: 700,
		pause: 8000
	});		

// Mobilemenu

$('#menuopen').click(function(){
$('#mainNav').slideDown();
$(this).hide();
$('#menuclose').show();
});

$('#menuclose').click(function(){
$('#mainNav').slideUp();
$('#menuopen').show();
$(this).hide();
});

//	Full høyde
	$('.flexslider .slides li, .slideshow').css('height', $(window).height())
	$(window).on("resize", function () {
		$('.flexslider .slides li, .slideshow').css('height', $(window).height())
	});

	$('.fullHeight').css('min-height', $(window).height())
	$(window).on("resize", function () {
		$('.fullHeight').css('min-height', $(window).height())
	});

// Slideshow
	$(function(){
		$(".slideshow li, .subPageTop, .imgSwap").each(function(){
			  var d = $(this), i = d.find('img').first();
			  if(i.length) {
				  d.css({
					  'background-image' : 'url(' + i.attr('src') + ')'
				  });
				  i.remove();
			  }
		  });
		  });
		  

// Accordion
		$('#accordion').find('.accordion-toggle').click(function(){

		  //Expand or collapse this panel
		  $(this).next().slideToggle('fast');

		  //Hide the other panels
		  $(".accordion-content").not($(this).next()).slideUp('fast');

		});


/* Quicktech */

// Left sidebar menu toggle on Mobile
$('#left-menu-toggle').click( function()
{
	$(this)
		.toggleClass('open')
		.siblings('ul').slideToggle(500);
});

// Butikkutvalg category pages: click to show the item's data on the box below
$('.butikkListing li span').click( function()
{
	var $item = $(this);

	$('.butikkListing li span').removeClass('active');
	$item.addClass('active');

	$('#item-title').text( $item.data('name').toUpperCase() );
	$('#item-text').html( $item.data('text') );
	$('#item-image').attr( 'src', $item.data('image') )
});

// Ignore empty anchors
$('a[href="#"]').click( function(e)
{
	e.preventDefault();
});

// Search toggle on the main menu
$('.search-toggle').click( function()
{
	$('.search-form').slideToggle(300);
});

// Ignore empty searches
$('.search-form').submit( function(e)
{
	if( $(this).find('[type="search"]').val() == '' )
	{
		e.preventDefault();
	}
});

/* Cart functions */

// Default 'cart get' on page load
getCart();

// 'Kjøp' buttons
$('.add-to-cart-btn').click( function()
{
	var $btn = $(this);

	if( $btn.hasClass('disabled') )
	{
		return false;
	}

	$product_container = $btn.parents('.shop-product-container');

	var pid = $btn.data('product-id');
	var pnm = $product_container.find('.product-name').text();
	var pex = $product_container.find('.product-excerpt').text();
	var pim = $product_container.find('.product-image').attr('src');
	var ppr = $product_container.find('[data-price]').data('price');
	var pli = $product_container.find('[data-product-url]').data('product-url');
	var min = $product_container.find('[min]').attr('min');

	var qty = $product_container.find('.product-qty').val();

	$btn
		.text('Laster...')
		.addClass('disabled');

	$.post
	(
		'/@cart/add',
		{
			product : pid +';'+ pnm +';'+ pex +';'+ pim +';'+ ppr +';'+ pli +';'+ min,
			amount  : qty
		},
		function( response )
		{
			if( response.messages.error == '' )
			{
				getCart( function()
				{
					$('.handlekurv').slideDown();

					$btn
						.text('Kjøp')
						.removeClass('disabled');
				});
			}
		}
	);
});

// Increase/decrease qty buttons
var min_reached = false;
$('.shop-product-container .antall i').click( function()
{
	var $input = $(this).parents('.shop-product-container').find('.product-qty');

	var new_value = ( $(this).hasClass('fa-minus-circle') ) ? ( parseInt( $input.val() ) - 1 ) : ( parseInt( $input.val() ) + 1 );
	if( new_value == 0 ) {
		new_value = 1
	}
	$input.val( new_value );
});

/* Cart page */

// 'Empty cart' button
$('#empty-cart-btn').click( function()
{
	$.post
	(
		'/@cart/empty',
		{},
		function()
		{
			window.location = window.location;
		}
	);
});

// 'Remove product' buttons
$('.shopItem .close i').click( function()
{
	var $btn = $(this);
	var pid = $btn.parents('.shop-product-container').data('product-id');

	$.post
	(
		'/@cart/remove',
		{
			product: pid
		},
		function( response )
		{
			$btn.parents('.shop-product-container')
				.addClass('removed')
				.slideUp();

			getCart( function()
			{
				cart_updateTotalPrice();

				// Reload page if the cart is empty after this product removal
				if( $('#handlekurv .shop-product-container:not(.removed)').length == 0 )
				{
					location.href = location.href;
				}
			});
		}
	);
});

// 'Change quantity' products
$('#handlekurv .shopItem .antall i').click( function()
{
	var $p = $(this).parents('.shop-product-container');
	var pid = $p.data('product-id');
	var qty = $p.find('.product-qty').val();

	var change_qty = ( $(this).hasClass('fa-plus-circle') ) ? 1 : -1;

	var $input = $p.find('.product-qty');
	if( min_reached )
	{
		$input.val( parseInt( qty ) );
		return false;
	}

	$.post
	(
		'/@cart/add',
		{
			product : pid,
			amount  : change_qty
		},
		function()
		{
			var $price = $p.find('[data-cart-product-price]');
			$price.attr('data-cart-product-price', ( qty * $price.data('cart-product-single-price') ) );

			cart_updateTotalPrice();

			getCart();
		}
	);
});

// Freight management
$('input[name="hentelevere"]').change( function()
{
	if( $(this).attr('id') == 'levere' )
	{
		$('#freight-element').removeClass('removed').slideDown( function()
		{
			cart_updateTotalPrice();
		});
	}
	else
	{
		$('#freight-element').addClass('removed').slideUp( function()
		{
			cart_updateTotalPrice();
		});
	}
});

// Finish Order form
$('#finishOrderForm').submit( function(e)
{
	var $form = $(this);
	var $email      = $form.find('[type=email]');
	var $phone      = $form.find('[name=phone]');
	var $accept_chk = $form.find('#accept-checkbox');

	if( $email.val() == '' )
	{
		$email
			.focus()
			.addClass('form-error');

		
		var pos = $email.offset().top - $('header#top').outerHeight() - 21;
		$('html, body').animate({ scrollTop: pos }, 250);

		e.preventDefault();
		return false;
	}

	if( $phone.val() == '' )
	{
		$phone
			.focus()
			.addClass('form-error');

		
		var pos = $phone.offset().top - $('header#top').outerHeight() - 21;
		$('html, body').animate({ scrollTop: pos }, 250);

		e.preventDefault();
		return false;
	}

	if( ! $accept_chk.is(':checked') )
	{
		$accept_chk
			.focus()
			.siblings('label').addClass('form-error');

		var pos = $accept_chk.offset().top - $('header#top').outerHeight() - 21;
		$('html, body').animate({ scrollTop: pos }, 250);

		e.preventDefault();
		return false;
	}
});

/* Cart page */


function getCart( callback )
{
	$.get
	(
		'/@cart/get',
		{},
		function( response )
		{
			if( response.messages.error == '' )
			{
				var qty = response.total;

				if( qty > 0 )
				{
					$('#qty-in-cart, .cart-qty-icon').text( qty );
					$('#qty-in-cart').text( $('#qty-in-cart').text() +' vare'+ ( ( parseInt( qty ) > 1 ) ? 'r' : '' ) );
					$('.cart-qty-icon').fadeIn();
				}
				else
				{
					$('.cart-qty-icon').fadeOut();
				}

				if( callback )
				{
					callback( response );
				}
			}
		}
	);
}

function cart_updateTotalPrice()
{
	var total = 0;

	$('.shopItem:not(.removed)').each( function()
	{
		total += parseInt( $(this).find('[data-cart-product-price]').attr('data-cart-product-price') );
	});

	$('#cart-total-price').text('Totalt kr. '+ total +',-');
}
/* Cart functions */

/* Quicktech */